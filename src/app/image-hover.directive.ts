import { Directive, HostListener, Renderer2, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[appImageHover]'
})
export class ImageHoverDirective {

  @Input() appImageHover;
  constructor(public elementRef: ElementRef,
    public renderer: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
    if (this.appImageHover) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'src', this.appImageHover.url);
      // this.renderer.addClass(this.elementRef.nativeElement, 'active');
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.appImageHover) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'src', this.appImageHover.thumbnailUrl);
      // this.renderer.removeClass(this.elementRef.nativeElement, 'active');
    }
  }

}
