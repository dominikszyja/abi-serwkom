import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export const baseUrl = 'https://jsonplaceholder.typicode.com';
export interface PhotoModel {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  url = '/photos';
  constructor(private httpClient: HttpClient) {
    this.url = baseUrl + this.url;
  }

  getImages(pageIndex, pageSize = 12): Observable<any> {
    let query = '?';
    if (pageIndex) {
      query += '_page=' + pageIndex;
    }
    if (pageSize) {
      query += '&_limit=' + pageSize;
    }
    return this.httpClient.get<Array<PhotoModel>>(this.url + query, { observe: 'response' });
  }


  parseLinkHeader(header) {
    if (header.length === 0) {
      throw new Error('input must not be of zero length');
    }

    const result = [];
    // Split parts by comma and parse each part into a named link
    return header.split(/(?!\B"[^"]*),(?![^"]*"\B)/).reduce((links, part) => {
      const section = part.split(/(?!\B"[^"]*);(?![^"]*"\B)/);
      if (section.length < 2) {
        throw new Error('section could not be split on \';\'');
      }
      const url = section[0].replace(/<(.*)>/, '$1').trim();
      const q = {};
      url.split('?')[1].split('&').forEach(i => {
        q[i.split('=')[0]] = i.split('=')[1];
      });

      const name = section[1].replace(/rel="(.*)"/, '$1').trim();
      result.push({ url, params: q, name });

      return result;
    }, {});
  }
}
