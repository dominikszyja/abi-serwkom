import { Component, OnInit, OnDestroy } from '@angular/core';
import { ImageService } from '../image.service';
import { HttpResponse } from '@angular/common/http';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {


  pageIndex = 1;
  pageSize = 12;

  imageData;
  pagination;
  activeImage;
  isLoading = true;

  _isRefreshAvailable = true;
  refreshSubject = new Subject();
  subscription: Subscription;

  constructor(private imageService: ImageService) {

    this.subscription = this.refreshSubject.pipe(debounceTime(300)).subscribe(x => {
      this._isRefreshAvailable = true;
    })
  }


  getImages() {
    this.isLoading = true;

    this.imageService.getImages(this.pageIndex, this.pageSize).subscribe((res: HttpResponse<any>) => {
      const _l = res.headers.get('link');
      const link = this.imageService.parseLinkHeader(_l);
      if (link) {
        this.pagination = link;
      }
      if (res) {
        this.imageData = res.body;
      }
    }, err => { }, () => {
      this.isLoading = false;
    });
  }

  onPage(params) {
    if (this._isRefreshAvailable) {
      this._isRefreshAvailable = false;
      this.pageIndex = params._page;
      this.pageSize = params._limit;

      this.getImages();
    }
    this.refreshSubject.next()

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.getImages();
  }

}
